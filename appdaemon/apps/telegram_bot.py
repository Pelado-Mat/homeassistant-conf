import appdaemon.plugins.hass.hassapi as hass
 
 
class TelegramBotEventListener(hass.Hass):
    """Event listener for Telegram bot events."""
 
    def initialize(self):
        """Listen to Telegram Bot events of interest."""
        self.listen_event(self.receive_telegram_command, 'telegram_command')
        self.listen_event(self.receive_telegram_callback, 'telegram_callback')
 
## COMANDOS
    def porton_command(self, user_id):
        if self.get_state(self.args["portonConStatus"]) == "on":
            state = self.get_state(self.args["porton"])
        else:
            state = "disconnected"

        if state == "disconnected":
            msg = "El porton esta desconectado"
            keyboard = []
            
        elif state == "closed" or state == "off":
            msg = "El Porton esta actualmente cerrado"
            keyboard = [[("Abrir", "/abrir_porton"), ("No hacer nada",
                                                    "/do_nothing")]]
        else:
            msg = "El Porton esta actualmente abierto"
            keyboard = [[("Cerrar", "/cerrar_porton"),
                         ("No hacer nada", "/do_nothing")]]
 
        self.call_service(
            'telegram_bot/send_message',
            target=user_id,
            message=msg,
            disable_notification=True,
            inline_keyboard=keyboard)
    
    def estado_porton_command(self, user_id):
        if self.get_state(self.args["portonConStatus"]) == "on":
            state = self.get_state(self.args["porton"])
        else:
            state = "disconnected"

        if state == "disconnected":
            msg = "El porton esta desconectado"
        elif state == "closed" or state == "off":
            msg = "El Porton esta actualmente cerrado"
        else:
            msg = "El Porton esta actualmente abierto"
 
        self.call_service(
            'telegram_bot/send_message',
            target=user_id,
            message=msg)

    def estado_estanque_command(self, user_id):
        pct = self.get_state(self.args["estanqueMK"])
        msg = "El estanque MK esta al %s %%" % pct
 
        self.call_service(
            'telegram_bot/send_message',
            target=user_id,
            message=msg)
        
        self.call_service(
            'telegram_bot/send_message',
            target=user_id,
            disable_notification=True,
            inline_keyboard=keyboard)
 

   
        
    def receive_telegram_command(self, event_id, payload_event, *args):
        assert event_id == 'telegram_command'
        user_id = payload_event['user_id']
        chat_id = payload_event['chat_id']
        command = payload_event['command']
 
        if command == "/porton":
            self.porton_command(chat_id)
        elif command == "/estado_porton":
            self.estado_porton_command(chat_id)
        elif command == "/estado_estanques":
            self.estado_estanque_command(chat_id)


    def receive_telegram_callback(self, event_id, payload_event, *args):
        assert event_id == 'telegram_callback'
        data_callback = payload_event['data']
        callback_id = payload_event['id']
        chat_id = payload_event['chat_id']
 
        if data_callback == '/abrir_porton':
            self.call_service('telegram_bot/edit_replymarkup',
                message_id='last',
                chat_id=chat_id,
                inline_keyboard=[])
            self.call_service(
                'telegram_bot/answer_callback_query',
                message='Abriendo el Porton',
                callback_query_id=callback_id)
            self.call_service(
                'cover/open_cover',
                entity_id=self.args["porton"])
 
        elif data_callback == '/cerrar_porton':
            self.call_service('telegram_bot/edit_replymarkup',
                message_id='last',
                chat_id=chat_id,
                inline_keyboard=[])
            self.call_service(
                'telegram_bot/answer_callback_query',
                message='Cerrando el Porton!',
                callback_query_id=callback_id)
            self.call_service(
                'cover/close_cover',
                entity_id=self.args["porton"])
 
        elif data_callback == '/do_nothing':
            self.call_service('telegram_bot/edit_replymarkup',
                message_id='last',
                chat_id=chat_id,
                inline_keyboard=[])
            self.call_service(
                'telegram_bot/answer_callback_query',
                message='OK, no hago nada',
                callback_query_id=callback_id)

        elif data_callback == '/estanqueMK_acknowledge':
            self.call_service('telegram_bot/edit_replymarkup',
                message_id='last',
                chat_id=chat_id,
                inline_keyboard=[])
            self.call_service(
                'telegram_bot/answer_callback_query',
                message='Ok',
                callback_query_id=callback_id)
            self.call_service(
                'alert.turn_off',
                entity_id='alert.alerta_estanque_mk_bajo'
                )    

